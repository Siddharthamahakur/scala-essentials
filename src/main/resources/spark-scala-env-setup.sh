## Java
sudo apt-get update
sudo apt-get install default-jdk

## Scala
sudo apt-get remove scala-library scala
sudo wget https://downloads.lightbend.com/scala/2.12.3/scala-2.12.3.deb
sudo dpkg -i scala-2.12.3.deb
sudo apt-get update
sudo apt-get install scala

## SBT
echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
sudo apt-get update
sudo apt-get install sbt

##github desktop ubuntu
sudo wget https://github.com/shiftkey/desktop/releases/download/release-2.6.3-linux1/GitHubDesktop-linux-2.6.3-linux1.deb
sudo gdebi GitHubDesktop-linux-2.6.3-linux1.deb

##git commands
git check-ignore -v $(find . -type f -print)
git rm -r --cached *.class


#Ubuntu
https://www.cyberciti.biz/faq/change-theme-in-ubuntu/