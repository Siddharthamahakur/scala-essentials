package com.sid.properties

import com.typesafe.config._


object ReadProperties extends App {
  val config: Config = ConfigFactory.load()

  val dir = config.getString("app.data.dir")
  val driver = config.getString("jdbc.driver")
  val url = config.getString("jdbc.url")
  val username = config.getString("jdbc.username")
  val password = config.getString("jdbc.password")
  println(s"driver =   $driver")
  println(s"url =      $url")
  println(s"username = $username")
  println(s"password = $password")
  println(s"directory = $dir")


}


