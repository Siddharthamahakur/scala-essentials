package com.sid.spark3

import org.apache.spark.sql.SparkSession


trait SparkEnv extends Serializable {
  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local[*]")
      .appName("scala-essentials")
      .getOrCreate()
  }
}
