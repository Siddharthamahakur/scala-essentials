package com.sid.spark3.cassandra

import com.sid.spark3.SparkEnv
import org.apache.log4j.Logger

object CassandraSink extends SparkEnv{
  @transient lazy val logger: Logger = Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {

    logger.info("SparkSession created")
    val readBooksDF = spark
      .read
      .format("org.apache.spark.sql.cassandra")
      .options(Map("table" -> "student", "keyspace" -> "analytics"))
      .load
    readBooksDF.show(truncate = false)

  }
}
